# README #

This template serves as a starting point and standardization point for java libraries built for MasterControl.

### Why use this project? ###
* You are building libraries like everyone else at MasterControl
* Hooks your library up so you can use any MasterControl library or from MavenCentral
* Great feedback and protections are builtin
  * Static Code Analysis
  * Unit Testing
  * Code Coverage
  * Java Doc
* When this project is improved with more tools and features, you just need to sync. 


### How do I get set up? ###
* See http://mcustsec/wiki/doku.php?id=dev_in_java_project_creation

## If creating a ReST endpoint ##
# Setup #
In order allow the test server to inject database credentials into your endpoint there is some configuration required. There are two ways to configure this: 1. Fill in the EncryptedTestValueMap_example class with the encrypted credentials for the database and any other config options required (EFP, etc). Then rename the class to EncryptedTestValueMap. 2. Add the path to your application service in the installedServices folder. Make sure that your EncryptedTestValueMap class is renamed to EncryptedTestValueMap_example. Example: D:\\Source\\TeamBranch\\installedServices\\applicationATOG

# How to use #
Create your rest endpoints under src/main/java/com/mastercontrol/your/package/
Click the run icon in the TestAppServer class.
The endpoint will be accessible at {your-machine-main}:8088/reverseproxy/{the-path-to-your-enpoint}
The path and/or port can be changed inside TesAppServer by changing the default value: http://0.0.0.0:8088/reverseproxy/. You should match your site's reverse proxy's path.

### Who do I talk to? ###

* Colby for now

### See http://mcustsec/wiki/doku.php?id=dev_in_java for more details ###