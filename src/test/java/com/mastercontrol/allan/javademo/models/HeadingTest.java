package com.mastercontrol.allan.javademo.models;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by atribe on 12/13/2016.
 * Testing the heading object
 */
public class HeadingTest {
    @Test
    public void testHeadingCreation() {
        Heading heading = new Heading();
        final String theBuild = "TheBuild";
        heading.setBuild(theBuild);
        heading.setTimezone("MDT");
        heading.setGmtTime("09:55");
        heading.setPart(3);
        final String thisVersion = "ThisVersion";
        heading.setGameversion(thisVersion);


        assertEquals(theBuild, heading.getBuild());
        assertEquals(thisVersion, heading.getGameversion());

        assertEquals(heading, heading);
    }
}
