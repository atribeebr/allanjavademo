package com.mastercontrol.allan.javademo.models;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by atribe on 12/13/2016.
 * Testing the journal class
 */
public class JournalTest {
    @Test
    public void testJournalCreation() {
        Journal journal = new Journal();
        journal.setHeading(new Heading());
        journal.setLoadGame(new LoadGame());
        journal.setLocation(new Location());
        Progress progress = new Progress();
        progress.setCombat(5);
        progress.setCqc(1);
        progress.setEmpire(10);
        progress.setFederation(5);
        progress.setTrade(43);
        journal.setProgress(progress);
        journal.setRank(new Rank());

        assertEquals(journal, journal);
        assertEquals(journal.getProgress().getCombat(), 5);
    }
}
