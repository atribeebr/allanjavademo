package com.mastercontrol.allan.javademo.services;


import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/reverseproxy/")
public class RestApiConfig extends ResourceConfig {

    /**
     * Configuration for REST application.
     */
    public RestApiConfig() {
        packages("com.mastercontrol");
    }
}
