package com.mastercontrol.allan.javademo.services;

import com.mastercontrol.allan.javademo.models.Journal;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by atribe on 12/9/2016.
 * Testing the journal loader.
 */
public class JournalLoaderTest {
    JournalLoader journalLoader;

    @Before
    public void setup() {
        this.journalLoader = new JournalLoader();
    }

    @Test
    public void testReadingSuppliedFileName() throws Exception {
        Journal journal = journalLoader.readFile("simple.journal.json");

        assertEquals("r113955", journal.getHeading().getBuild());
    }

    @Test
    public void testReadingDefaultFileName() throws Exception {
        Journal journal = journalLoader.readDefaultFile();

        assertEquals("r113955", journal.getHeading().getBuild());
    }
}