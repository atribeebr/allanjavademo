package com.mastercontrol.allan.javademo.services;

import com.mastercontrol.allan.javademo.models.LoadGame;
import org.junit.*;
import org.junit.runners.MethodSorters;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by atribe on 12/13/2016.
 * For testing my ORM entity
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LoadGameDAOTest {
    private static LoadGameDAO loadGameDAO;

    @BeforeClass
    public static void setUp()throws Exception{
        loadGameDAO = new LoadGameDAO("orm.properties");
    }

    @AfterClass
    public static void tearDown() throws Exception{
        loadGameDAO = null;
    }

    @Test
    public void testGettingCommanderNameById() {
        System.out.println("DB Test has started.");
        String commanderName = loadGameDAO.getCommanderFromId(1);
        assertEquals("dizhen", commanderName);
    }

    @Test
    public void testAAAGetAllRows() {
        System.out.println("DB Test 2 has started.");
        List<LoadGame> commanders= loadGameDAO.getCommanders();

        List<LoadGame> expectedCommanders = new ArrayList<>();
        expectedCommanders.add(new LoadGame(1, "dizhen", "anaconda", 1));
        expectedCommanders.add(new LoadGame(2, "allan", "sidewinder", 2));

//        for (LoadGame loadGame: commanders) {
//            loadGame.setJournal(null);
//        }
        assertEquals(expectedCommanders, commanders);
    }

    @Test
    public void testBBBPersistLoadGame() {
        LoadGame loadGame = new LoadGame(null, "DeleteMe", "Viper", 3);
        loadGameDAO.persistLoadGame(loadGame);

        assertNotNull(loadGame.getId());
    }

    @Test(expected = NoResultException.class)
    public void testCCCDeleteLoadGame() {
        LoadGame loadGame = loadGameDAO.getLoadGameByCommander("DeleteMe");
        assertNotNull(loadGame);
        loadGameDAO.deleteLoadGame(loadGame);
        loadGameDAO.getLoadGameByCommander("DeleteMe"); //throws the NoResultException
    }

    @Test
    public void testUpdateLoadGameShip() {
        LoadGame loadGame = loadGameDAO.updateLoadGameShipById(1, "corvette");
        assertEquals("corvette", loadGame.getShip());
        loadGameDAO.updateLoadGameShipById(1, "anaconda");
    }
}
