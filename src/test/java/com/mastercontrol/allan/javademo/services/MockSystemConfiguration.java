package com.mastercontrol.allan.javademo.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastercontrol.SystemConfigurationAdapter.UglySystemConfigurationInterfaceDoNotUse;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;


@Component("UglySystemConfiguration")
class MockSystemConfiguration implements UglySystemConfigurationInterfaceDoNotUse {

    private Map<String, String> config = new HashMap<>();

    MockSystemConfiguration() {
        try {
            config = (HashMap)Class.forName("com.mastercontrol.EncryptedTestValueMap").newInstance();
            System.out.println("Loaded MasterControl config from EncryptedTestValueMap.");
        } catch (Exception ex) {
            System.out.println("Loading MasterControl config from File.");
            config = loadFromFile();
        }
    }

    private Map<String, String> loadFromFile(){
        Map<String, String> loadedConfig = new HashMap<>();

        Properties properties = new Properties();
        try(FileInputStream is = new FileInputStream("project.properties") ) {
            properties.load(is);
        } catch (IOException ex) {
            System.err.println("MASTERCONTROL: Issue loading 'project.properties' file.");
            ex.printStackTrace();
            return new HashMap<>();
        }

        if(!properties.containsKey("applicationLocation")) {
            System.out.println("MASTERCONTROL: Properties 'project.properties' does not contain an applicationLocation value.");
            return new HashMap<>();
        }

        File conflocation = new File(properties.getProperty("applicationLocation"));

        System.out.println("Loading config from: " + conflocation.getAbsolutePath());

        ObjectMapper mapper = new ObjectMapper();

        findRightFiles(conflocation).stream().filter(Optional::isPresent).forEach(file -> {
            try {
                loadedConfig.putAll(mapper.readValue(file.get(), HashMap.class));
            } catch (IOException ex) {
                System.out.println("MASTERCONTROL: Unable to open MasterControl application folder or get data.");
                ex.printStackTrace();
            }
        });

        if(loadedConfig.size() == 0) {
            System.out.println("MASTERCONTROL: No MasterControl configuration was found.");
        } else {
            System.out.println("MASTERCONTROL: Loaded MasterControl configuration successfully.");
        }
        return loadedConfig;
    }

    private List<Optional<File>> findRightFiles(final File currentFile) {
        if("datasource.conf".equals(currentFile.getName())) {
            return Arrays.asList(Optional.of(currentFile));
        } else if("EFP.conf".equals(currentFile.getName())) {
            return Arrays.asList(Optional.of(currentFile));
        } else {
            if(currentFile.isDirectory()) {
                List<Optional<File>> foundFiles = new ArrayList<>();

                for(File subFile : currentFile.listFiles()) {
                    foundFiles.addAll(findRightFiles(subFile));
                }
                return foundFiles.stream().filter((Optional::isPresent)).collect(Collectors.toList());
            }

            return Arrays.asList(Optional.empty());
        }
    }

    @Override
    public String getValueOrDefault(final String superkey, final String key, final String defaultValue) {
        return config.get(key);
    }

    @Override
    public String getValueOrThrowException(final String superkey, final String key) {
        return config.get(key);
    }

    @Override
    public void set(final String superkey, final String key, final String value) {

    }

    @Override
    public void removeKey(final String superkey, final String key) {

    }

    @Override
    public void removeAllKeys(final String superkey) {

    }

    @Override
    public void reload(final String superkey) {

    }
}