package com.mastercontrol.allan.javademo.services;

import java.util.HashMap;

//In order to use this as the MasterControl configuration when testing
// this endpoint rename this class to EncryptedTestValueMap_example
class EncryptedTestValueMap extends HashMap<String, String> {
    EncryptedTestValueMap() {

        put("databaseName", "QD0iKVNJXUVZIzYkLEwqJS8xSl8iWkNXMEQtTSBaIFNFRllDNDhJJi9aIlAgCg=="); //testUpgrade
        put("host", "QF43OyQ0O145MUYjSUpaVUQuNlEyUDxQKz8zS0M1VDo1L0UlNT8rJC8lVUAgCg==");
        put("password", "QDpJVDklNFE3SDtOMDlYSS48PjdLWUxNSFo4S1BbSl0vWjkhJTU1QTcmVTQgCg==");
        put("type", "MFdfRVYsRF5XViwmVkBBQkBKLVpNVTAgIAo=");
        put("port", "MCQnOzJDUSVFRl5YXFooWlhNMic8KiAgIAo=");
        put("username", "MD1FJC1AVl1ISVJLPVIiUitdRUpXIyAgIAo=");
        put("path", "\\\\mainman\\EFP\\");
    }


}
