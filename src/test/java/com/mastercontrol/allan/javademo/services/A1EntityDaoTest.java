package com.mastercontrol.allan.javademo.services;

import com.mastercontrol.allan.javademo.models.A1Entity;
import com.mastercontrol.allan.javademo.models.A2Entity;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by atribe on 12/19/2016.
 */
public class A1EntityDaoTest {
  private static A1EntityDao a1EntityDao;

  @BeforeClass
  public static void setUp()throws Exception{
    a1EntityDao = new A1EntityDao("orm.properties");
  }

  @AfterClass
  public static void tearDown() throws Exception{
    a1EntityDao = null;
  }

  @Test
  public void testFindAll() {
    List<A1Entity> expectedOutput = new ArrayList<>();
    A1Entity allan = new A1Entity(1, "Allan", "Tribe", "Blue", null);
    A1Entity miriam = new A1Entity(2, "Miriam", "Tribe", "Dirt", null);
    A1Entity ben = new A1Entity(3, "Ben", "Henderson", "Green", null);

    A2Entity springville = new A2Entity(1, "Springville", "Utah", "United States", miriam);
    miriam.setA2Id(springville);

    A2Entity denver = new A2Entity(2, "Denver", "Colorado", "United States", ben);
    ben.setA2Id(denver);

    A2Entity ogden = new A2Entity(3, "Ogden", "Utah", "United States", allan);
    allan.setA2Id(ogden);

    expectedOutput.add(allan);
    expectedOutput.add(miriam);
    expectedOutput.add(ben);

    List<A1Entity> actualResults = a1EntityDao.findAllA1Entity();
    assertEquals(expectedOutput.get(2).getFirstName(), actualResults.get(2).getFirstName());
  }

  @Test
  public void testPersistOneToOne() {
    A1Entity dizhen = new A1Entity(null, "StillNotDizhen", "Tribe", "Royal Blue", null);
    A2Entity taiwan = new A2Entity(null, "StillNotYongHe", "Taibei County", "Taiwan", dizhen);
    dizhen.setA2Id(taiwan);

    A1Entity persistedDizhen = a1EntityDao.save(dizhen);

    assertEquals(dizhen, persistedDizhen);
  }

}
