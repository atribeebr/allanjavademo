package com.mastercontrol.allan.javademo.services;

import com.mastercontrol.SystemConfiguration;
import com.mastercontrol.SystemConfigurationAdapter.UglySystemConfigurationInterfaceDoNotUse;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import java.net.URI;

import java.util.logging.Level;
import java.util.logging.Logger;

public class TestAppServer { private static final URI BASE_URI = URI.create("http://0.0.0.0:8088/reverseproxy/");

    /**
     * Main method that spins up Grizzly to develop on.
     * @param args The command line arguments passed (not used)
     */
    public static void main(final String[] args) {
        try {
            final RestApiConfig config = new RestApiConfig();

            AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
            context.refresh();
            @SuppressWarnings("deprecation") UglySystemConfigurationInterfaceDoNotUse badStuff = new MockSystemConfiguration();
            SystemConfiguration sysConf = new com.mastercontrol.SystemConfigurationAdapter.SystemConfigurationAdapter();
            context.getBeanFactory().registerSingleton("UglySystemConfiguration", badStuff);
            context.getBeanFactory().registerSingleton("SystemConfiguration", sysConf);
            context.register(SpringConfig.class);
            context.refresh();
            config.property("contextConfig", context);
            config.property("contextConfigLocation", "com.mastercontrol.TestAppServer");

            final HttpServer server = GrizzlyHttpServerFactory.createHttpServer(BASE_URI, config, true);

            Runtime.getRuntime().addShutdownHook(new Thread(server::shutdownNow));

            System.out.println(String.format("Application started.%nStop the application using CTRL+C"));

            Thread.currentThread().join();
        } catch (Exception ex) {
            Logger.getLogger(TestAppServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
