package com.mastercontrol.allan.javademo.services;

import com.mastercontrol.allan.javademo.models.JournalDBObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by atribe on 12/14/2016.
 * Testing the JournalDAO
 */
public class JournalDAOTest {
    private JournalDAO journalDAO;

    @Before
    public void setUp() throws Exception{
        journalDAO = new JournalDAO("orm.properties");
    }

    @After
    public void tearDown() throws Exception{
        journalDAO = null;
    }

    @Test
    public void testGettingAJournal() {
        JournalDBObject journal = journalDAO.getJournalById(1);
        assertEquals(11, journal.getRank().getTrade());
    }

    @Test
    public void testManualJoin() {
        List<JournalDBObject> journalList = journalDAO.doingAJoinMethodManually(11);
        assertEquals(11, journalList.get(0).getRank().getTrade());
    }

}
