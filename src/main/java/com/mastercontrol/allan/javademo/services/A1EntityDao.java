package com.mastercontrol.allan.javademo.services;

import com.mastercontrol.allan.javademo.models.A1Entity;
import com.mastercontrol.datasource.DatasourceConfiguration;
import com.mastercontrol.orm.util.DB;
import org.springframework.context.annotation.Lazy;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by atribe on 12/19/2016.
 * A1Entity DAO
 */
public class A1EntityDao {
  private EntityManager em;

  @Lazy
  @Resource(name = "DatasourceConfiguration")
  private DatasourceConfiguration dsConfig;

  /**
   * Constructor that connects to the db.
   */
  public A1EntityDao() {
    this.em = new DB(dsConfig).getEntityManager();
  }

  /**
   * Constructor that takes a string.
   * @param configFileName config file name
   */
  public A1EntityDao(final String configFileName) {
    this.em = new DB(configFileName).getEntityManager();
  }

  /**
   * Find all A1.
   * @return a1 list
   */
  public List<A1Entity> findAllA1Entity() {
    return em.createQuery("SELECT a1 FROM A1Entity a1", A1Entity.class).getResultList();
  }

  /**
   * Save an A1Entity.
   * @param a1 object to save
   * @return the persisted a1entity object
   */
  public A1Entity save(final A1Entity a1) {
    em.getTransaction().begin();
    em.persist(a1);
    em.getTransaction().commit();
    return a1;
  }
}
