package com.mastercontrol.allan.javademo.services;

import com.mastercontrol.allan.javademo.models.JournalDBObject;
import com.mastercontrol.datasource.DatasourceConfiguration;
import com.mastercontrol.orm.util.DB;
import org.springframework.context.annotation.Lazy;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by atribe on 12/14/2016.
 * Data Access Object for the Journal Object
 */
public class JournalDAO {
    private EntityManager em;

    @Lazy
    @Resource(name = "DatasourceConfiguration")
    private DatasourceConfiguration dsConfig;

    /**
     * Constructor that connects to the db.
     */
    public JournalDAO() {
        this.em = new DB(dsConfig).getEntityManager();
    }

    /**
     * Constructor that takes a string.
     * @param configFileName config file name
     */
    public JournalDAO(final String configFileName) {
        this.em = new DB(configFileName).getEntityManager();
    }

    /**
     * Gets a journal object by id.
     * @param id id of the journal object
     * @return the journal object
     */
    public JournalDBObject getJournalById(final int id) {
        return em
                .createQuery("SELECT j FROM JournalDBObject AS j WHERE j.id=:id", JournalDBObject.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    /**
     * Testing using a manual join.
     * @param tradeRank the tradeRank
     * @return list of journal objects
     */
    public List doingAJoinMethodManually(final int tradeRank) {
        return em
                .createQuery("SELECT j FROM JournalDBObject j INNER JOIN j.rank r WHERE r.trade=:tradeRank")
                .setParameter("tradeRank", tradeRank)
                .getResultList();
    }
}
