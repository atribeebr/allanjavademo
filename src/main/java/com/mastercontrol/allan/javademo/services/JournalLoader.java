package com.mastercontrol.allan.javademo.services;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastercontrol.allan.javademo.models.Journal;

import java.io.File;
import java.io.IOException;

/**
 * Journal loader class.
 */
public class JournalLoader {
    private ObjectMapper mapper = new ObjectMapper();
    private String defaultFileName = "simple.journal.json";

    /**
     * Default Constructor that configures the mapper.
     */
    public JournalLoader() {
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
    }

    /**
     * Reads the journal file passed in.
     * @param fileName String of the filename.
     * @return Journal object that represents the file.
     * @throws IOException if there is a bad file name.
     */
    public Journal readFile(final String fileName) throws IOException {
        //Get file from resources folder
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        return mapper.readValue(file, Journal.class);
    }

    /**
     * Reads the default json file.
     * @return Journal object that represents the file.
     * @throws IOException if there is a bad file name.
     */
    public Journal readDefaultFile() throws IOException {
        return readFile(defaultFileName);
    }
}
