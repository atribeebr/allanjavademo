package com.mastercontrol.allan.javademo.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Created by atribe on 12/19/2016.
 * k
 */
@Entity
@Data
@Table(name = "a_1")
@AllArgsConstructor
@NoArgsConstructor
public class A1Entity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @Column(name = "first_name")
  private String firstName;
  @Column(name = "last_name")
  private String lastName;
  @Column(name = "fav_color")
  private String favColor;
  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "a_2_id")
  private A2Entity a2Id;
}
