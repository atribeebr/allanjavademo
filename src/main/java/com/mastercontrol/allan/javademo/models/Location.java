package com.mastercontrol.allan.javademo.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Location object.
 */
@Data
@Entity
@Table(name = "allan_location", schema = "dbo", catalog = "mastercontrol")
public class Location {
    @Id
    private int id;
    private String starSystem;
    private float starPos;
    private String faction;
}
