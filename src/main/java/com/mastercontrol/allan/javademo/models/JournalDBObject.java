package com.mastercontrol.allan.javademo.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;
import java.util.Set;

/**
 * Journal object.
 */
@Entity
@Table(name = "allan_journal", schema = "dbo", catalog = "mastercontrol")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class JournalDBObject {
    /**
     * Default constructor.
     */
    @Id
    private int id;
    private int heading;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "journal", cascade = CascadeType.ALL)
    private Set<LoadGame> loadGame;
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "journal", cascade = CascadeType.ALL)
    private Rank rank;
//    private Progress progress;
//    private Location location;
    private int progress;
    private int location;

}
