package com.mastercontrol.allan.javademo.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Progress Object.
 */
@Entity
@Table(name = "allan_progress", schema = "dbo", catalog = "mastercontrol")
public class Progress extends AbstractScore {
    @Id
    private int id;
}
