package com.mastercontrol.allan.javademo.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import javax.persistence.FetchType;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * Rank object.
 */
@Entity
@Table(name = "allan_rank", schema = "dbo", catalog = "mastercontrol")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Rank {
    @Id
    private int id;
    private int combat;
    private int trade;
    private int explore;
    private int empire;
    private int federation;
    private int cqc;
    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private JournalDBObject journal;

//    /**
//     * To string method.
//     * @return a string
//     */
//    @Override
//    public String toString() {
//        return "Rank {id: " + getId() + "}";
//    }
}
