package com.mastercontrol.allan.javademo.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by atribe on 12/19/2016.
 * k
 */
@Entity
@Table(name = "allan_user_table", schema = "dbo", catalog = "mastercontrol")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AllanUserTable {
  @Id
  private int id;
  @Column(name = "user_id")
  private String userId;
  @Column(name = "test_value")
  private String testValue;
}
