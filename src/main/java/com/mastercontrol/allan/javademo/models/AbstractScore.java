package com.mastercontrol.allan.javademo.models;

import lombok.Data;
import lombok.ToString;

/**
 * Score object.
 */
@Data
@ToString
public abstract class AbstractScore {
    private int id;
    private int combat;
    private int trade;
    private int explore;
    private int empire;
    private int federation;
    private int cqc;
}
