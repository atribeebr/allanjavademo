package com.mastercontrol.allan.javademo.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * Elite Dangerous Journal File Header.
 */
@Data
public class Heading {

    private int id;
    @JsonProperty("localdatetime")
    private LocalDateTime localDateTime;
    private String timezone;
    @JsonProperty("gmt_time")
    private OffsetTime gmtTime;
    private int part;
    private String gameversion;
    private String build;

    /**
     * Custom setter that takes a string. Used in deserilization of the json file.
     * @param localDateTime string value of local date time.
     */
    public void setLocalDateTime(final String localDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yy-MM-dd-HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(localDateTime, formatter);
    }

    /**
     * Custom setter that takes a string. Used in deserilization of the json file.
     * @param gmtTime string value of gmt date time.
     */
    public void setGmtTime(final String gmtTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime dateTime = LocalTime.parse(gmtTime, formatter);
        this.gmtTime = OffsetTime.of(dateTime, ZoneOffset.UTC);
    }
}
