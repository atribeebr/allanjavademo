package com.mastercontrol.allan.javademo.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Created by atribe on 12/19/2016.
 * k
 */
@ToString(exclude = "a1Id")
@Entity
@Data
@Table(name = "a_2", schema = "dbo", catalog = "mastercontrol")
@AllArgsConstructor
@NoArgsConstructor
public class A2Entity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  private String city;
  private String state;
  private String country;
  @OneToOne(targetEntity = A1Entity.class, cascade = CascadeType.ALL)
  @JoinColumn(name = "a_1_id")
  private A1Entity a1Id;
}
