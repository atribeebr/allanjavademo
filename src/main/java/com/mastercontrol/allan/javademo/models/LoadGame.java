package com.mastercontrol.allan.javademo.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * LoadGame event object.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "allan_load_game", schema = "dbo", catalog = "mastercontrol")
public class LoadGame {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String commander;
    private String ship;
//    @OneToOne(fetch = FetchType.LAZY)
//    @PrimaryKeyJoinColumn
    private int journal;

    /**
     * To string method.
     * @return a string
     */
    public String toString() {
        return "Rank {id: " + id + " commander: " + commander + " ship: " + ship + "}";
    }
}
