package com.mastercontrol.allan.javademo.rest;

import com.mastercontrol.allan.javademo.services.JournalLoader;
import com.mastercontrol.allan.javademo.services.LoadGameDAO;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * Created by atribe on 12/12/2016.
 * ReST endpoint to get the load game status.
 */
@Path("/javademo")
public class LoadEndpoint {

    /**
     * Journal Rest Endpoint.
     * @return Journal object as json
     */
    @PermitAll
    @GET
    @Path("/load")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCommanderName() {
        JournalLoader theJournalLoader = new JournalLoader();

        try {
            return Response.status(Response.Status.OK)
                    .entity(theJournalLoader.readDefaultFile())
                    .build();
        } catch (IOException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Gets the commander given an id.
     * @param id the commander id.
     * @return commander object.
     */
    @PermitAll
    @GET
    @Path("/commander")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCommander(@QueryParam("id") final String id) {
        LoadGameDAO loadGameDAO = new LoadGameDAO();

        return Response.status(Response.Status.OK)
                .entity(loadGameDAO.getCommanderFromId(Integer.valueOf(id)))
                .build();
    }
}
