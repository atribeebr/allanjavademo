package com.mastercontrol;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = SystemConfiguration.class)
class SpringConfig {
}
